# CSE525 Work

Contains my work on the class.

## Folder Structure

cse525/
	documentation/ has all the relevant documentation for the hardware and arm instruction set.
	projects/X has my project work for project X.
	projects/2 contains work on raspberry-pi-os lessons.
		lessonN/ contains work for lesson N.
			exercises/X contains work for exercise X.
	
Note: Video demos for each lesson exercise of project 2 are in cse525/documentation/projects/2/lessonN/exercises/X.

## To Do:

1. lesson3
2. lesson4
3. lesson5
4. Add build files to .gitignore.
