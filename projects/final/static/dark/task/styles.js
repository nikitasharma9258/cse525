import { PATH , MODE } from './VAR' ;
export default ( gulp , plugins ) => ( ) =>
	gulp . src ( PATH . STYLE_IN )
		. pipe ( MODE . DEV ? plugins . sourcemaps . init ( ) : plugins . noop ( ) )
		. pipe ( plugins . sass ( ) . on ( 'error' , plugins . sass . logError ) )
		. pipe ( plugins . postcss ( [ plugins . autoprefixer ( ) , plugins . cssnano ( ) ] ) )
		. pipe ( MODE . DEV ? plugins . sourcemaps . write ( './maps' ) : plugins . noop ( ) )
		. pipe ( gulp . dest ( 'build' ) ) ;
