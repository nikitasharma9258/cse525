const APP_DATA = require ( '../package.json' ) ;

const MODE = {
	DEV : process . env . NODE_ENV !== 'production' ,
	WATCH : process . env . WATCH !== 'true' ,
} ;

const PATH = {
	NODE_MOD : 'node_modules' ,
	VIEWS : 'src/views' ,
	VIEW_IN : 'src/views/*.pug' ,
	VIEW_OUT : 'build/views/' ,

	STYLE_IN	: 'src/dark.scss' ,
	STYLE_OUT	: 'build/*.css' ,

	PRE_SCRIPTS_IN	: 'src/dark.pre.js' ,
	PRE_SCRIPTS_OUT	 : 'dark.pre.js' ,
	SCRIPTS_IN	: 'src/dark.js' ,
	SCRIPTS_OUT	: 'dark.js' ,

	SERVER_IN	: [ 'src/app.js' , 'src/server.js' ] ,
	SERVER_OUT	: 'build/*.js' ,
} ;

export { APP_DATA , MODE , PATH } ;
