import { MODE , PATH } from './VAR' ;
export default ( gulp , plugins ) => ( done ) => {
	const STARTUP_TIMEOUT = 5000 ;

	const server = plugins . nodemon ( {
		script : 'build/server.js' ,
		stdout : false ,
		env : { 'NODE_ENV' : 'development' , 'PORT' : '3000' } ,
	} ) ;
	let starting = false ;

	const onReady = function ( ) {
		starting = false ;
		done ( ) ;
	} ;
	server . on ( 'start' , function ( ) {
		starting = true ;
		setTimeout ( onReady , STARTUP_TIMEOUT ) ;
	} ) ;

	server . on ( 'stdout' , function ( stdout ) {
		process . stdout . write ( stdout ) ;
		if ( starting ) { onReady ( ) ; }
	} ) ;
}
