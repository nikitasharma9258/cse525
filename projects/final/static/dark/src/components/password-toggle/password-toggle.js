import $ from 'jquery' ;

function passwordToggle ( parent , children ) {

	const CHECKBOX		= parent + '>div>' + children [ 0 ] ;
	const INPUT		= parent + '>div>' + children [ 1 ] ;
	const CONFIRM_INPUT	= parent + '>'     + children [ 2 ] ;

	$ ( document ) . ready ( ( ) => {
		$ ( CHECKBOX) . on ( 'click' , event => {
			if ( $ ( CHECKBOX ) . prop ( 'checked' ) == true ) {

				$ ( INPUT )		. attr ( 'type' , 'text' ) ;
				$ ( CONFIRM_INPUT )	. attr ( 'type' , 'text' ) ;

			} else if ( $ ( CHECKBOX ) . prop ( 'checked' ) == false ) {

				$ ( INPUT )		. attr ( 'type' , 'password' ) ;
				$ ( CONFIRM_INPUT )	. attr ( 'type' , 'password' ) ;

			}
		} ) ;
	} ) ;
}
passwordToggle ( '.password-hide-1' , [ '.checkbox' , '.password' , '.confirm-password' ] ) ;
passwordToggle ( '.password-hide-2' , [ '.checkbox' , '.password' , '.confirm-password' ] ) ;
passwordToggle ( '.password-hide-3' , [ '.checkbox' , '.password' , '.confirm-password' ] ) ;
passwordToggle ( '.password-hide-4' , [ '.checkbox' , '.password' , '.confirm-password' ] ) ;
passwordToggle ( '.password-hide-5' , [ '.checkbox' , '.password' , '.confirm-password' ] ) ;

export default passwordToggle ;
