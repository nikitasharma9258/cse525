// Applies visibility hidden to the main column as default styling.
var style = document.createElement('style');
var style_text = document.createTextNode('#main-column{visibility:hidden;}');
style . appendChild ( style_text ) ;
document.getElementsByTagName('head')[0].append( style ) ;
