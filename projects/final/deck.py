import random

class deck:
    def __init__(self):
        self.cards = {
            0: 4,
            1: 4,
            2: 4,
            3: 4,
            4: 4,
            5: 4,
            6: 4,
            7: 4,
            8: 4,
            9: 4,
            10: 4,
            11: 4,
            12: 4,
            13: 4,
        }

    def retrieve_available_cards(self):
        available_cards = [ ]
        for card in self.cards:
            if self.cards[card] != 0:
                available_cards.append(card)
        return available_cards

    def retrieve_random_available_card(self):
        available_cards = self.retrieve_available_cards()

        # Checks for empty list.
        if ( available_cards is None ) or ( len(available_cards) == 0 ):
            return None

        random.seed()
        

        index = random.randint(0, len(available_cards) - 1)
        card_number = available_cards[index]

        self.cards[card_number] -= 1

        return available_cards[index]

    def reset(self):
        for card in self.cards:
            self.cards[card] = 4

    def print_un4values(self):
        for card in self.cards:
            if self.cards[card] != 4:
                print("cards[" + str(card) + "] = " + str(self.cards[card]))

if __name__ == '__main__':

    dealer_deck = deck()
    card = 0
    while card < 14:
        dealer_deck.cards[card] = 0
        card += 1
    print(str(dealer_deck.retrieve_available_cards()))
    print(str(dealer_deck.retrieve_random_available_card()))
