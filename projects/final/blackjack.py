import random

class deck:
    def __init__(self):
        self.cards = {
            0: 4,
            1: 4,
            2: 4,
            3: 4,
            4: 4,
            5: 4,
            6: 4,
            7: 4,
            8: 4,
            9: 4,
            10: 4,
            11: 4,
            12: 4,
            13: 4,
        }

    def retrieve_available_cards(self):
        available_cards = [ ]
        for card in self.cards:
            if self.cards[card] != 0:
                available_cards.append(card)
        return available_cards

    def retrieve_random_available_card(self):
        available_cards = self.retrieve_available_cards()

        # Checks for empty list.
        if ( available_cards is None ) or ( len(available_cards) == 0 ):
            return None

        random.seed()
        

        index = random.randint(0, len(available_cards) - 1)
        card_number = available_cards[index]

        self.cards[card_number] -= 1

        return available_cards[index]

    def reset(self):
        for card in self.cards:
            self.cards[card] = 4

    def print_un4values(self):
        for card in self.cards:
            if self.cards[card] != 4:
                print("cards[" + str(card) + "] = " + str(self.cards[card]))

def clear_game(deck, dealer_history, player_history, move):
    input("Clearing hands and restarting deck after enter!")
    deck.reset()
    dealer_history.clear()
    player_history.clear()

if __name__ == '__main__':

    dealer_history = [ ]
    player_history = [ ]

    dealer_deck = deck()

    # 1 for hit.
    # 0 for halt.
    print("--------------------------------------------------")
    print("Instructions: On a move, hit me is 1, else halt.")

    print("--------------------------------------------------")
    move = int(input("move: "))
    player_sum = 0
    dealer_sum = 0

    while True:

        # Hit
        if move == 1:
            # Retrieve card from deck.
            card = dealer_deck.retrieve_random_available_card()

            if card is None:
                print("No more cards!")
            
            player_sum += card

            # Ace Rules
            if card == 0:
                ace_value = 0
                while True:
                    ace_value = int(input("Ace = 1 or Ace = 11? "))
                    if ace_value == 1 or ace_value == 11: break
                card = ace_value


            player_history.append(card)
            print("Player history: " + str(player_history))
            print("Player sum: " + str(player_sum))

            # Checks if player's hand sums to <= 21
            if player_sum <= 21:
                move = int(input("move: "))
            else:
                # Dealer automatically wins if player busts.
                move = 0
                print("Dealer won: player's hand busted!")
                clear_game(dealer_deck, dealer_history, player_history, move)
                player_sum = 0
                dealer_sum = 0
                move = int(input("move: "))

            print("--------------------------------------------------")

        # Halt: retrieve dealer's hand.
        else:
            print("Dealer's Turn!")
            while dealer_sum <= 16:
                # Retrieve card from deck.
                card = dealer_deck.retrieve_random_available_card()

                if card is None:
                    print("No more cards!")

                # Ace Rules for dealer
                if card == 0:
                    future_dealer_sum = dealer_sum + 11

                    if future_dealer_sum > 21:
                        card = 1
                        dealer_sum += 1
                    else:
                        card = 11
                        dealer_sum = future_dealer_sum
                else:

                    dealer_sum += card

                dealer_history.append(card)
                print("dealer history: " + str(dealer_history))
                print("dealer sum: " + str(dealer_sum))
                print("--------------------------------------------------")

            if 21 < dealer_sum:
                print("Player won: dealer's hand bust!")
            elif player_sum < dealer_sum:
                print("Dealer won: player_sum < dealer_sum <= 21!")
            elif dealer_sum < player_sum:
                print("Player won: dealer_sum < player_sum <= 21!")
            elif dealer_sum == player_sum:
                print("Tie: player_sum = dealer_sum!")



            clear_game(dealer_deck, dealer_history, player_history, move)
            player_sum = 0
            dealer_sum = 0

            move = int(input("move: "))
