import random
from flask import Flask, render_template, url_for, request

app = Flask(__name__)

deck = {
        1: 4,
        2: 4,
        3: 4,
        4: 4,
        5: 4,
        6: 4,
        7: 4,
        8: 4,
        9: 4,
        10: 4,
        11: 4,
        12: 4,
        13: 4,
}

dealer_history = [ ]
player_history = [ ]

def retrieve_available_cards():
    global deck
    available_cards = [ ]
    for card in deck:
        if deck[card] != 0:
            available_cards.append(card)

    return available_cards

def retrieve_random_available_card():
    global deck

    available_cards = retrieve_available_cards()

    if not available_cards:
        return -1

    random.seed()
    
    card = random.randint(0, len(available_cards))

    deck[card] -= 1

    return available_cards[card]


# Default configuration has static files served in /static.
@app.route('/blackjack/home')
def home():
    return render_template('blackjack.html')

# Handles Game logic
@app.route('/blackjack/process-card-number', methods = ['POST', 'GET'])
def process_card_number():

    if request.method == 'POST':

        global player_history
        card_number = int(request.form['card_number'])

        if card_number < 14 and card_number > 0:

            # Is the card available in the deck?
            if deck[card_number] > 0 and deck[card_number] < 5:

                deck[card_number] -= 1
                
                player_history.append(card_number) # In
                player_sum = sum(player_history)


                dealer_sum = sum(dealer_history)

                print("--------------------------------------------------")
                print("Player Hit!")
                print("--------------------------------------------------")

                print("Player History:" + player_history)
                print(player_history)
                print("Player Sum:")
                print(player_sum)

                print("--------------------------------------------------")

                player_text1 = "player history: " + str(player_history)
                player_text2 = "player sum: " + str(player_sum)

                print("--------------------------------------------------")

                print("Dealer History:")
                print(dealer_history)
                print("Dealer Sum:")
                print(dealer_sum)

                print("--------------------------------------------------")

                print("Available Cards: " + retrieve_available_cards())

                print("--------------------------------------------------")

                server_text1 = "dealer history: " + str(dealer_history)
                server_text2 = "dealer sum: " + str(dealer_sum)

                return client_text1 + '\r' + client_text2 + '\r' + server_text1 + '\r' + server_text2


if __name__ == '__main__':
   app.run(debug = True)
