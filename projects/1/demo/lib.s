// David Castellon
// Project 1 Demo: lib.s
    .section ".text"
    .global add
add:
    add r0,r1
    bx lr

    .global sub
sub:
    sub r0,r1
    bx lr

    .global mult
mult:
    mul r0,r1
    bx lr

    .global u_div
u_div:
    // R0 = Numerator
    // R1 = Denominator
    // R2 = Quotient
    // R3 = Remainder
    // R4 = Bit Iterator

    push {r4, lr}  // Put r4 and lr onto stack and change stack pointer.
                   // r4 is not an argument, so if the previous function uses
                   // r4, then it will be lost if not pushed to stack.
    mov r2, #0     // 0 -> r2
    mov r3, #0     // 0 -> r3
    mov r4, #32    // 32 -> r4

    b .loop_check

    .loop:
        // Pseudo Instruction for lsll r0, r0 #1
        movs r0, r0, LSL #1  // Logical Shift left on r0 by 1 -> r0
        adc r3, r3, r3       // r3 <- r3 + r3

        cmp r3, r1           // Compares r3 and r1.
        subhs r3, r3, r1     // r3 <- r3 - r1
        adc r2, r2, r2       // r2 <- r2 + r2
    .loop_check:
        subs r4, r4, #1      // r4 <- r4 + #1
        bpl .loop

    movs r0, r2
    movs r1, r3
    pop {r4, lr}   // Pop r4 and lr from stack.
    bx lr          // Returns from subroutine.

    .global u_quo
u_quo:
    push {lr}
    bl u_div     // Quotient is in r0.
    pop {lr}
    bx lr

    .global u_rem
u_rem:
    push {lr}
    bl u_div     // Remainder is in r1.
    movs r0, r1
    pop {lr}
    bx lr

    .global s_div
s_div:
    push {r4, r5, lr}
    // r0 has numerator.
    // r1 has denominator.
    // r4 has numerator sign.
    // r5 has denominator sign.
    // r4 = #0x8000, iff r4 is negative.
    and r4, r0, #0x8000
    // r5 = #0x8000, iff r5 is negative.
    and r5, r1, #0x8000

    // Takes absolute value of a number.
    cmp r4, #0x0000
    beq .den_neg
    neg r0, r0
    .den_neg:
    cmp r5, #0x0000
    beq .un_div
    neg r1, r1
    
    .un_div:
    bl u_div    // Quotient is in r0.
                // Remainder is in r1.

    // Remainder Sign
    cmp r4, #0x0000
    beq .sign_result
    neg r1, r1

    .sign_result:
    eor r4, r4, r5
    cmp r4, #0x0000
    beq .popper
    neg r0, r0

    .popper:
    pop {r4, r5, lr}
    bx lr

    .global s_quo
s_quo:
    push {lr}
    bl s_div     // Quotient is in r0.
    pop {lr}
    bx lr

    .global s_rem
s_rem:
    push {lr}
    bl s_div     // Remainder is in r1.
    movs r0, r1
    pop {lr}
    bx lr
    .end
