// David Castellon
// Project 1 Demo: main.c
#include <stdio.h>

int add ( int arg1, int arg2 );
int sub ( int arg1, int arg2 );
int mult ( int arg1, int arg2 );
int s_quo ( int arg1, int arg2 );
int s_rem ( int arg1, int arg2 );

int my_scan(int *input) {
    if ( scanf("%d", input) == 0 ) {
        while ( getchar() != '\n' );
        printf("--------------------------------------------------\n");
        printf("!!!Please enter a correct input!!!\n");
        return 1;
    } ;
    return 0;
}

int main(int argc, char *argv[]) {

    unsigned int mode = 1;
    int arg1 = 0;
    int arg2 = 0;
    int result = 0;

    printf("--------------------------------------------------\n");
    printf("Simple Calculator\n");

    while ( mode != 0 ) {

        printf("--------------------------------------------------\n");
        printf("Select an Operation:\n");
        printf("0\tQuit\n");
        printf("1\tAddition\n");
        printf("2\tSubtraction\n");
        printf("3\tMultiplication\n");
        printf("4\tDivision\n");

        printf("--------------------------------------------------\n");
        printf("Operation: ");
        if ( my_scan(&mode) ) continue ;

        if ( mode == 0 ) {
            printf("--------------------------------------------------\n");
            printf("Goodbye!\n");
            printf("--------------------------------------------------\n");
            break;
        } else if ( mode > 4 ) {
            printf("Select Within the range!\n");
            continue;
        }
        
        printf("--------------------------------------------------\n");
        printf("Argument 1: ");
        if ( my_scan(&arg1) ) continue; // Newline placed by scan!

        printf("Argument 2: ");
        if ( my_scan(&arg2) ) continue;

        if ( mode == 1 ) {
            result = add(arg1, arg2);
        } else if ( mode == 2 ) {
            result = sub(arg1, arg2);
        } else if ( mode == 3 ) {
            result = mult(arg1, arg2);
        } else if ( mode == 4 ) {
            result = s_quo(arg1, arg2);
            int remainder = s_rem(arg1, arg2);

            printf("--------------------------------------------------\n");
            printf("Result = %d R %d\n", result, remainder);
            continue;
        }

        printf("--------------------------------------------------\n");
        printf("Result = %d\n", result);
    }

    return 0;
}
