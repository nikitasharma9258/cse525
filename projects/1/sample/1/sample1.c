// David Castellon
// Project 1 Sample 1: sample1.c

// Global Variables
signed   char var1 =   1 ;
unsigned char var2 =   2 ;
signed   int  var3 =   3 ;
unsigned int  var4 =   4 ;
const    int  num  = -10 ;

char wave[10] = "goodbye!!!" ; // 10 consecutive bytes.


void main()
{
	// Local Data Type
	int var5 = 5;
	// Various Loop Types
	for (var5;var5>0;var5--)
	{
		var1*=var1;
		var1/=var1;
		var1+=var1;
		var1-=var1;
	}

	do
	{
		var4-=1;
	}while(var4>0);

	while(var3 == 3)
	{
		var2 = var2;
		break;
	}
}
