@ ARMV6 instruction set.
	.arch armv6
@ EABI defines mapping between low-level concepts in high-level languages and
@ the specific abilities of hardware+OS.
	.eabi_attribute 28, 1
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4

	.file	"sample1.c"

@ Variable
@ Global: can be seen by compiler.
	.global	var1
	.data
	.type	var1, %object @ Symbol type is a data object.
	.size	var1, 1       @ Symbol size 1. (I think in bytes)
var1:
	.byte	1             @ Variable is a char, which
                              @ is 1 byte. Initialized to
                              @ value 1.

@ Variable
@ Global: Can be seen by compiler.
@ Variable is a 
	.global	var2
	.type	var2, %object
	.size	var2, 1
var2:
	.byte	2             @ Variable is a unsigned char,
                              @ which is 1 byte. Initialized to
                              @ value 2.

@ Variable
@ Global: Can be seen by compiler.
	.global	var3
	.align	2             @ Align to 4 bytes. Address % 4 == 0.
                              @ Done to speed up read/write time.
	.type	var3, %object
	.size	var3, 4
var3:
	.word	3             @ Variable is a signed int,
                              @ which is 4 bytes because 1 word is
                              @ 32 bits. Initialized to value 2.

@ Variable
@ Global: Can be seen by compiler.
	.global	var4
	.align	2
	.type	var4, %object
	.size	var4, 4
var4:
	.word	4             @ Variable is a unsigned int, which
                              @ is 4 bytes because 1 word is 32
                              @ bits. Initialized to value 4.

@ Variable
@ Global: Can be seen by compiler.
@ rodata: Read Only Data
	.global	num
	.section     .rodata
	.align	2
	.type	num, %object
	.size	num, 4
num:
	.word	-10           @ Variable is a const int,is 4 bytes
                              @ because 1 word is 32 bits. Initialized
                              @ to value 4.

@ Variable
@ Global: Can be seen by compiler.
@ Variable is a char array with length 10
@ Initialized to value "goodbye!!!"
	.global	wave
	.data
	.align	2
	.type	wave, %object
	.size	wave, 10      @ 10 bytes
wave:
	.ascii	"goodbye!!!"


@ Program Data
@ Global: Can be seen by compiler.
	.global	__aeabi_idiv
	.text
	.align	2
	.global	main
	.syntax unified
	.arm                       @ Selects arm instruction set.
	.fpu vfp                   @ Selects floating point unit
                                   @ to assemble for.
	.type	main, %function    @ Symbol function type.
main:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0

@ Push fp and lr to stack.
@ fp = frame pointer = address of where stack was.
@ lr = link register = address of return from function.
	push	{fp, lr}
@ fp <- sp + #4 (Updates frame pointer.)
	add	fp, sp, #4
@ sp <- sp - #8 (Stack Pointer back to start?)
	sub	sp, sp, #8
@ var5 local variable. Stored in the code segment.
@ Rather than the data segment.
	mov	r3, #5
@ fp + #-8 <- r3 (Places 5 into stack.)
	str	r3, [fp, #-8]
@ Goto L2
	b	.L2
@ For body.
.L3:
	ldr	r3, .L9
	ldrsb	r3, [r3]
	uxtb	r2, r3
	ldr	r3, .L9
	ldrsb	r3, [r3]
	uxtb	r3, r3
	smulbb	r3, r2, r3
	uxtb	r3, r3
	sxtb	r2, r3
	ldr	r3, .L9
	strb	r2, [r3]
	ldr	r3, .L9
	ldrsb	r3, [r3]
	mov	r2, r3
	ldr	r3, .L9
	ldrsb	r3, [r3]
	mov	r1, r3
	mov	r0, r2
	bl	__aeabi_idiv
	mov	r3, r0
	sxtb	r2, r3
	ldr	r3, .L9
	strb	r2, [r3]
	ldr	r3, .L9
	ldrsb	r3, [r3]
	uxtb	r2, r3
	ldr	r3, .L9
	ldrsb	r3, [r3]
	uxtb	r3, r3
	add	r3, r2, r3
	uxtb	r3, r3
	sxtb	r2, r3
	ldr	r3, .L9
	strb	r2, [r3]
	ldr	r3, .L9
	mov	r2, #0
	strb	r2, [r3]
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	str	r3, [fp, #-8]

@ For Condition
.L2:
@ fp + #-8 <- r3
	ldr	r3, [fp, #-8]
@ r3 == #0
	cmp	r3, #0
@ branch if greater than 0.
	bgt	.L3

@ Do While Loop
.L4:
	ldr	r3, .L9+4
	ldr	r3, [r3]
	sub	r3, r3, #1
	ldr	r2, .L9+4
	str	r3, [r2]
	ldr	r3, .L9+4
	ldr	r3, [r3]
@ Do While Condition
	cmp	r3, #0
	bne	.L4
	b	.L8

@ While loop
.L7:
	ldr	r3, .L9+8
	ldrb	r2, [r3]	@ zero_extendqisi2
	ldr	r3, .L9+8
	strb	r2, [r3]
	b	.L6
@ While loop Condition
.L8:
	ldr	r3, .L9+12
	ldr	r3, [r3]
	cmp	r3, #3
	beq	.L7


; Exit
.L6:
	nop
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, pc}
.L10:
	.align	2
.L9:
	.word	var1 @ Address of var1.
	.word	var4 @ Address of var2.
	.word	var2 @ Address of var3.
	.word	var3 @ Address of var4.
	.size	main, .-main
	.ident	"GCC: (Raspbian 6.3.0-18+rpi1+deb9u1) 6.3.0 20170516"
	.section	.note.GNU-stack,"",%progbits
