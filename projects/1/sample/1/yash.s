rch armv6
	.eabi_attribute 28, 1
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.file	"P1-1.c"
	.text
	.global	var1
	.data
	.type	var1, %object
	.size	var1, 1
var1:
	.byte	1
	.global	var2
	.type	var2, %object
	.size	var2, 1
var2:
	.byte	2
	.global	var3
	.align	2
	.type	var3, %object
	.size	var3, 4
var3:
	.word	3
	.global	var4
	.align	2
	.type	var4, %object
	.size	var4, 4
var4:
	.word	4
	.global	num
	.section	.rodata
	.align	2
	.type	num, %object
	.size	num, 4
num:
	.word	-10
	.global	wave
	.data
	.align	2
	.type	wave, %object
	.size	wave, 10
wave:
	.ascii	"goodbye!!!"
	.text
	.align	2
	.global	main
	.arch armv6
	.syntax unified
	.arm
	.fpu vfp
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!					//store fp(r11) to sp(r13) - 4
	add	fp, sp, #0					//fp(r11) = sp(r13) + 0
	sub	sp, sp, #12					//sp(r13) = sp(r13) - 12
	mov	r3, #5						//copies the value of 5 into r3
	str	r3, [fp, #-8]					//store r3 to fp(r11) - 8
	b	.L2
.L3:
	ldr	r3, .L9						//
	ldrsb	r3, [r3]						//load signed byte to r3 from r3		
	uxtb	r2, r3						//zero extend byte (in our case r2 will     be extended by the value of r3)
	ldr	r3, .L9						//
	ldrsb	r3, [r3]						//load signed byte to r3 from r3
	uxtb	r3, r3						//zero extend byte (in our case r3 will be extended by the value of r3)
	smulbb	r3, r2, r3					//
	uxtb	r3, r3						//zero extend byte (in our case r3 will be extended by the value of r3)
	sxtb	r2, r3						//
	ldr	r3, .L9						//
	strb	r2, [r3]						//store byte from r2 to address r3
	ldr	r3, .L9						//
	mov	r2, #1						//copies the value of 1 into r2
	strb	r2, [r3]						//store byte from r2 to address r3
	ldr	r3, .L9						//
	ldrsb	r3, [r3]						//load signed byte to r3 from r3
	uxtb	r3, r3						//zero extend byte (in our case r3 will be extended by the value of r3)
	lsl	r3, r3, #1					//
	uxtb	r3, r3						//zero extend byte (in our case r3 will be extended by the value of r3)
	sxtb	r2, r3						//
	ldr	r3, .L9						//
	strb	r2, [r3]						//store byte from r2 to address r3
	ldr	r3, .L9						//
	mov	r2, #0						//copies the value of 0 into r2
	strb	r2, [r3]						//store byte from r2 to address r3
	ldr	r3, [fp, #-8]					//load r3 from fp(r11) - 8
	sub	r3, r3, #1					//r3 = r3 - 1
	str	r3, [fp, #-8]					//store r3 to fp(r11) - 8
.L2:
	ldr	r3, [fp, #-8]					//load r3 from fp(r11) - 8
	cmp	r3, #0						//compares the value in r3 with 0
	bgt	.L3						//Branches to .L3 if source register is greater than zero
.L4:
	ldr	r3, .L9+4					//
	ldr	r3, [r3]						//load r3 from the address in r3
	sub	r3, r3, #1					//r3 = r3 - 1
	ldr	r2, .L9+4					//
	str	r3, [r2]						//store r3 to r2
	ldr	r3, .L9+4					//
	ldr	r3, [r3]						//load r3 from the address in r3
	cmp	r3, #0						//compares the value in r3 with 0
	bne	.L4						//branches to .L4 if Z flag is cleared
	b	.L8						//branches to .L8
.L7:
	ldr	r3, .L9+8					//
	ldrb	r2, [r3]	@ zero_extendqisi2			//load byte into r2 from r3 (zero top 3 bytes)
	ldr	r3, .L9+8					//
	strb	r2, [r3]						//store byte from r2 to address r3
	b	.L6						//branches to .L6											
.L8:
	ldr	r3, .L9+12					//
	ldr	r3, [r3]						//load r3 from the address in r3
	cmp	r3, #3						//compares the value in r3 with 3
	beq	.L7						//jumps to .L7 if zero flag is set
.L6:
	nop							//no operation, does nothing
	add	sp, fp, #0					//sp(r13) = fp(r11) + 0
	@ sp needed
	ldr	fp, [sp], #4					//load from fp(r11), then sp(r13) = sp(r13) + 4
	bx	lr						//branches to the address contained in lr(r14)
.L10:
	.align	2
.L9:
	.word	var1
	.word	var4
	.word	var2
	.word	var3
	.size	main, .-main
	.ident	"GCC: (Raspbian 8.3.0-6+rpi1) 8.3.0"
	.section	.note.GNU-stack,"",%progbits


