@ David Castellon
@ Project 1 Sample 4: next_char
	.section ".text"
	.global next_char
next_char:
	ADD r0,#1 @ How is the ARM calling convention obeyed here?
	MOV pc,lr
	.end
