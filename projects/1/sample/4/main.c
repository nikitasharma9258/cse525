// David Castellon
// Project 1 Sample 4: main.c
// Compile the program by entering ‘gcc main.c next_char.s’ and check for errors.

#include <stdio.h>
unsigned char next_char(char in);
void main()
{
	printf("Next Character= %c\n",next_char('A'));
}
