#include "sched.h"
#include "irq.h"
#include "printf.h"

static struct task_struct init_task = INIT_TASK;
struct task_struct *current = &(init_task);
struct task_struct * task[NR_TASKS] = {&(init_task), };
int nr_tasks = 1;

void print_cpu_context(struct cpu_context *cpu) {
	printf("------------------------------\n\r");
	printf("Cpu Context:\n\r");
	printf("------------------------------\n\r");
	printf("x20: %d\n\r", cpu->x20);
	printf("x21: %d\n\r", cpu->x21);
	printf("x22: %d\n\r", cpu->x22);
	printf("x23: %d\n\r", cpu->x23);
	printf("x24: %d\n\r", cpu->x24);
	printf("x25: %d\n\r", cpu->x25);
	printf("x26: %d\n\r", cpu->x26);
	printf("x27: %d\n\r", cpu->x27);
	printf("x28: %d\n\r", cpu->x28);
	printf("fp: %d\n\r", cpu->fp);
	printf("sp: %d\n\r", cpu->sp);
	printf("pc: %d\n\r", cpu->pc);
}

void print_task(struct task_struct *task) {
	printf("------------------------------\n\r");
	printf("Task Information:\n\r");
	printf("state: %d\n\r", task->state);
	printf("counter: %d\n\r", task->counter);
	printf("priority: %d\n\r", task->priority);
	printf("preempt_count: %d\n\r", task->preempt_count);
	print_cpu_context(&(task->cpu_context));
}


void preempt_disable(void)
{
	current->preempt_count++;
}

void preempt_enable(void)
{
	current->preempt_count--;
}

void _schedule(void)
{
	preempt_disable();
	int next,c;
	struct task_struct * p;
	while (1) {
		c = -1;
		next = 0;
		for (int i = 0; i < NR_TASKS; i++){
			p = task[i];
			if (p && p->state == TASK_RUNNING && p->counter > c) {
				c = p->counter;
				next = i;
			}
		}
		if (c) {
			break;
		}
		for (int i = 0; i < NR_TASKS; i++) {
			p = task[i];
			if (p) {
				p->counter = (p->counter >> 1) + p->priority;
			}
		}
	}
	switch_to(task[next]);
	preempt_enable();
}

void schedule(void)
{
	current->counter = 0;
	_schedule();
}

void switch_to(struct task_struct * next) 
{
	struct task_struct * p;
	printf("\n\r------------------------------\n\r");
	printf("Switching Tasks\n\r");
	for(int t=0; t < NR_TASKS; t++) {
		p = task[t];
		printf("------------------------------\n\r");
		printf("task number: %d\n\r", t);
		print_task(p);
	}
	printf("task output: ");
	
	if (current == next) 
		return;
	struct task_struct * prev = current;
	current = next;
	cpu_switch_to(prev, next);
}

void schedule_tail(void) {
	preempt_enable();
}


void timer_tick()
{
	--current->counter;
	if (current->counter>0 || current->preempt_count >0) {
		return;
	}
	current->counter=0;
	enable_irq();
	_schedule();
	disable_irq();
}
