#include "utils.h"
#include "printf.h"
#include "peripherals/timer.h"

const unsigned int interval = 20000000;
unsigned int curVal = 0;

void timer_init ( void )
{
//	curVal = get32(TIMER_CLO);
//	curVal += interval;
//	put32(TIMER_C1, curVal);
	// Sets up timer interval.
	put32(TIMER_CTRL, LOC_TIMER_EN | interval);
}

void handle_timer_irq( void ) 
{
//	curVal += interval;
//	put32(TIMER_C1, curVal);
//	put32(TIMER_CS, TIMER_CS_M1);
//	printf("System Timer!\n\r");
	printf("Local Timer!\n\r");
	put32(TIMER_FLAG, LOC_TIMER_FLAG_CLR_RE);		// clear interrupt flag and reload timer

}
