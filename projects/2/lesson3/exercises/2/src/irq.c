#include "utils.h"
#include "printf.h"
#include "timer.h"
#include "mini_uart.h"
#include "entry.h"

#include "peripherals/irq.h"
#include "peripherals/mini_uart.h"

const char *entry_error_messages[] = {
	"SYNC_INVALID_EL1t",
	"IRQ_INVALID_EL1t",		
	"FIQ_INVALID_EL1t",		
	"ERROR_INVALID_EL1T",		

	"SYNC_INVALID_EL1h",		
	"IRQ_INVALID_EL1h",		
	"FIQ_INVALID_EL1h",		
	"ERROR_INVALID_EL1h",		

	"SYNC_INVALID_EL0_64",		
	"IRQ_INVALID_EL0_64",		
	"FIQ_INVALID_EL0_64",		
	"ERROR_INVALID_EL0_64",	

	"SYNC_INVALID_EL0_32",		
	"IRQ_INVALID_EL0_32",		
	"FIQ_INVALID_EL0_32",		
	"ERROR_INVALID_EL0_32"	
};

void enable_interrupt_controller()
{
	// Enables ARM Timer IRQ.
	// bit 29 enables uart interrupt
	put32(ENABLE_IRQS_1, SYSTEM_TIMER_IRQ_1 | (1<<29));
}

void show_invalid_entry_message(int type, unsigned long esr, unsigned long address)
{
	printf("%s, ESR: %x, address: %x\r\n", entry_error_messages[type], esr, address);
}

void handle_irq(void)
{
	unsigned int irq1 = get32(IRQ_PENDING_1);
	unsigned int irq2 = get32(AUX_IRQ);

	if (irq1 & SYSTEM_TIMER_IRQ_1) handle_timer_irq();
	if (irq2 & (1 << 0)) handle_mini_uart_irq();
}
