# Notes

Linker script is written in linker command language.
Linker script describes how the sections in the input files should be mapped into
the output file, and to control the memory layout of the output file.

# Raspberry Pi Startup Sequence

1. Turn On
2. GPU starts
3. GPU reads config.txt from boot partition.
4. kernel8.img is loaded into memory and executed.

## config.txt

	// Specifies kernel image should be loaded at address 0.
	kernel_old=1
	// Instructs GPU to not pass any command line arguments to the booted image.
	disable_commandline_tags=1
