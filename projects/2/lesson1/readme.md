# linux files

* arch    - This folder contains subfolders, each for a specific processor architecture. Mostly we are going to work with arm64 - this is the one that is compatible with ARM.v8 processors.
* init    - Kernel is always booted by architecture specific code. But then execution is passed to the start\_kernel function that is responsible for common kernel initialization and is an architecture independent kernel starting point. start\_kernel function, together with some other initialization functions, is defined in the init folder.
* kernel  - This is the core of the Linux kernel. Almost all major kernel subsystems are implemented there.
* mm      - All data structures and methods related to memory management are defined there.
* drivers - This is the largest folder in the Linux kernel. It contains implementations of all device drivers.
* fs      - You can look here to find different filesystem implementations.

# Default Macros

make -p : Prints out default macros.

* $ is symbol for getting variable.
* $(CC) is compiler
* $(CFLAGS) is compiler flags
* $@ is the name of the file to be made.
* $? is the names of the changed dependents.
* $< the name of the related file that caused the action.
* $* the prefix shared by target and dependent files.

```
	# Assume files main.cpp, hello.cpp, factorial.cpp, and functions.h exist.
	# Example:
	# Note: Make can automatically create a .o file, using cc -c on the
	# corresponding .c file. These rules are built-in the make. If you
	# indicate the .h files in the dependency line of the Makefile on which the
	# current target is dependent on, make will know that the corresponding
	# .cfile is already required. You do not have to include the command for
	# the compiler.

	# Prerequisite: main.o, factorial.o, and hello.o
	# When main.o, factorial.o, and hello.o are seen,
	# main.o factorial.o, and hello.o are maked.
	hello: main.o factorial.o hello.o
		$(CC) main.o factorial.o hello.o -o hello

	# main.cpp and functions.h are prerequisites.
	main.o: main.cpp functions.h
		$(CC) -c main.cpp

	# factorial.cpp and functions.h are prerequisites.
	factorial.o: factorial.cpp functions.h
		$(CC) -c factorial.cpp

	# hello.cpp and functions.h are prerequisites.
	hello.o: hello.cpp functions.h
		$(CC) -c hello.cpp

	clean:
		-rm *.o *~ core paper

	# Custom suffix rules in makefile.
