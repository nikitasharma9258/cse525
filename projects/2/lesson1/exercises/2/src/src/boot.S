#include "mm.h"

.section ".text.boot"

.globl _start

_start:
    // Only lets one processor execute master.
    // Pi3 has 4 processors.
    // All other processors hang = loop indefinitely.
    mrs    x0, mpidr_el1    // Retrieves processorID from mpidr_el1 system register.
    and    x0, x0, #0xff    // Checks if processorID is 0.
    cbz    x0, master       // If processorID is 0,     goto master.
    b      proc_hang        // If processorID is not 0, hang.


// Loop indefinitely.
proc_hang:
    b    proc_hang

// 
master:
    // bss_begin -> x0
    adr    x0, bss_begin
    // bss_end   -> x1
    adr    x1, bss_end
    // bss_end - bss_begin -> x1
    sub    x1, x1, x0
    // Goto memzero function.
    // Previous instructions set the function values for memzero c function.
    // memzero first argument is pointer to memory block initial value.
    // memzero second argument is memory block length.
    // memzero cleans bss section.
    bl     memzero

    // Initialize stack pointer.
    mov    sp, #LOW_MEMORY
    // Goto kernel main.
    bl     kernel_main
