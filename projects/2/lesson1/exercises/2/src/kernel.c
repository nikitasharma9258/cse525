// #include "mini_uart.h"
#include "uart.h"

void kernel_main(void)
{
	const int baudrate = 9600;
	uart_init(baudrate);
	uart_send_string("Exercise 2: baudrate = 9600\r\n");

	while (1) {
		uart_send(uart_recv());
	}
}
