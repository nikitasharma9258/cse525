#include "utils.h"
#include "peripherals/uart.h"
#include "peripherals/gpio.h"

void uart_send (char c)
{
	// Block while tx fifo is empty.
	while (get32(UART_FR)&(1<<5));

	put32(UART_DR, c);
}

char uart_recv (void)
{
	// Block while rx fifo is empty.
	while (get32(UART_FR)&(1<<4)) ;

	return (get32(UART_DR)&0xFF);
}

void uart_send_string(char *str)
{
	for (int i = 0; str[i] != '\0'; i++)
		uart_send((char)str[i]);
}

// uart init
void uart_init ( int baudrate )
{
	unsigned int selector;
	const int uart_clock = 48000000 ; // 48MHz UART Clock.

	// baudrate registers
	// c integer division removes fractional component.
	int baudrate_reg_whole = ( uart_clock / ( 16 * baudrate ) ) ;
	// frac_digits_in_binary = ( 2^6 ) * frac_part + (1/2)
	// baudrate_reg_frac expression is simplified.
	int baudrate_reg_frac = ( 8 * uart_clock + baudrate ) / ( 2 * baudrate ) ;
	
	// Reads the value at memory address GPFSEL1.
	selector = get32(GPFSEL1);

	// GPIO14 and 15 on ALT0 or UART.
	selector &= ~(7<<12);        // Clears bits 12-14.
	selector |=  (4<<12);        // Sets bit 14.
	selector &= ~(7<<15);        // Clears bits 15-17.
	selector |=  (4<<15);        // Sets bit 17.
	put32(GPFSEL1, selector);
	
	// GPIO pull down requires delays.
	put32(GPPUD,0);
	delay(150);
	put32(GPPUDCLK0,(1<<14)|(1<<15));
	delay(150);
	put32(GPPUDCLK0,0);

	put32(UART_CR, 0);        // Disables UART.
	put32(UART_IMSC, 0); // Disables UART interrupts.

	put32(UART_IBRD, baudrate_reg_whole); // Sets baudrate whole part.
	put32(UART_FBRD, baudrate_reg_frac) ; // Sets baudrate frac part.

	// Configuring UART Settings in variable selector to be loaded later in register.
	// Sets 8 bits and enables fifo.
	put32(UART_LCRH, (3<<5) | (1<<4));  // Loads UART Line Configurations.
	// Enables rx, tx, and uart.
	put32(UART_CR, (3<<8) | (1<<0));    // Loads UART Configurations.
}

void putc ( void *p, char c)
{
	uart_send(c);
}
