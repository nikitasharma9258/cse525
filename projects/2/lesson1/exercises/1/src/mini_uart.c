#include "utils.h"
#include "peripherals/mini_uart.h"
#include "peripherals/gpio.h"

void uart_send ( char c )
{
	while(!(get32(AUX_MU_LSR_REG)&0x20));
	put32(AUX_MU_IO_REG,c);
}

char uart_recv ( void )
{
	while(!(get32(AUX_MU_LSR_REG)&0x01));
	return(get32(AUX_MU_IO_REG)&0xFF);
}

void uart_send_string(char* str)
{
	for (int i = 0; str[i] != '\0'; i ++)
		uart_send((char)str[i]);
}

void uart_init ( int baudrate )
{
	unsigned int selector;
	const int sys_clock_freq = 250000000; // 250MHz
	int baudrate_reg = sys_clock_freq / ( 8 * baudrate ) - 1;

	selector = get32(GPFSEL1);

	// Cleans   gpio14     gpio15.
	selector &= ~(7<<12) | ~(7<<15); // Clears GPIOs.
	selector |=   2<<12  |   2<<15 ; // Gets GPIOs 14 and 15 on ALT0 == rx0
					 // and tx0 respectively.
	put32(GPFSEL1,selector);

	// Removes pull-up and pull-down states from a pin.
	put32(GPPUD,0);                   // Configures pull up or down.
	delay(150);                       // Required due to protocol.
	put32(GPPUDCLK0,(1<<14)|(1<<15)); // Clocks the control signal into the GPIO pads.
	delay(150);                       // Required due to protocol.
	put32(GPPUDCLK0,0);               // Removes Clock.

	put32(AUX_ENABLES,1);                // Enable mini uart (this also enables access to it registers)
	put32(AUX_MU_CNTL_REG,0);            // Disable auto flow control and disable receiver and transmitter (for now)
	put32(AUX_MU_IER_REG,0);             // Disable receive and transmit interrupts
	put32(AUX_MU_LCR_REG,3);             // Enable 8 bit mode
	put32(AUX_MU_MCR_REG,0);             // Set RTS line to be always high
	put32(AUX_MU_BAUD_REG,baudrate_reg); // Set baud rate to 115200

	put32(AUX_MU_CNTL_REG,3);         //Finally, enable transmitter and receiver
}


// This function is required by printf function
void putc ( void* p, char c)
{
	uart_send(c);
}
