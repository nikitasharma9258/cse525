#include "mini_uart.h"

void kernel_main(void)
{
	int baudrate = 9600;
	uart_init(baudrate);

	// String manipulation functions not defined, so hardcoded for now.
	uart_send_string("Exercise 1: Baudrate = 9600\r\n");

	while (1) {
		uart_send(uart_recv());
	}
}
