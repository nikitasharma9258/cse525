# Processor Initialization

# Exception Levels(EL)

Processor execution mode in which only a subset of all operations and registers is available.
There are 4 EL.

A program cannot have its EL raised without the assistance of a program with an already raised EL.

# EL0

Least privileged EL is EL0.

1. Mostly uses general purpose registers (x0-x30) and sp.
2. Can also use str and ldr commands.
3. User processes should only operate in EL0.

# EL1

OS operates usually at EL1.

1. Access to registers that allows configuring virtual memory settings.
2. Access to some system registers.

# EL2 and EL3

EL2 used when using a hypervisor. Host OS running in EL2 while guest OS running in EL1
EL3 used when transitions from ARM "Secure World" to "Insecure world"
"Insecure World" cannot access information from "Secure World" implemented in hardware lv.

# Debugging kernel

No debugging utils, so can only utilize printf.
printf code is pulled from somewhere else. Requires putc to be implemented.

# Exception Steps

Exception Link Register (ELR\_ELn) stores address of current instruction during an exception.
Saved Program Status Register (SPSR\_ELn) stores current processor state.

1. Address of the current instruction is saved in the ELR\_ELn register (Exception link register).
2. Current processor state is stored in SPSR\_ELn register (Saved Program Status Register).
3. An exception handler is executed and does whatever job it needs to do.
4. Exception handler calls eret instruction. This instruction restores processor state from SPSR\_ELn and resumes execution starting from the address, stored in the ELR\_ELn register.

* Exception handler must store state of general purpose registers.
* Exception handler does not have to return to the same location from which the exception originates.

# Switching to EL1

OS is not required to switch to EL1, but EL1 has the right set of privileges to implement all
common OS tasks.

# Processor State

* Condition Flags
* Interrupt disable bits
* Some Other bits.
