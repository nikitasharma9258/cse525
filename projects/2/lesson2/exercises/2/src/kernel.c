#include "printf.h"
#include "utils.h"
#include "mini_uart.h"

void print_exception_lv(void)
{
	printf("Exception Level: %d\r\n", get_el());
}

void kernel_init(void)
{
	uart_init();          // Initializes uart.
	init_printf(0, putc); // Initializes printf.
	print_exception_lv();
}

void kernel_main(void)
{
	print_exception_lv();
	while (1) uart_send(uart_recv());
}
