#include<stdio.h>

// Depending on OS division by zero is handled differently.

int main() {
	int i = 0;
	float k = 0;
	printf("i before %d\n", i);
	i = i / i ;
	printf("i after %d\n", i);

	printf("k before %d\n", k);
	k = k / k ;
	printf("k after %d\n", k);
}
